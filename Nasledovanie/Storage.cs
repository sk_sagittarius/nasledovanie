﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasledovanie
{
    public abstract class Storage
    {
        protected string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        protected string model;
        public string Model
        {
            get { return model; }
            set { model = value; }
        }

        public int Memory { get; set; }
        public int comingFiles;



        //public int infoCopy;
        public int InfoCopy
        {
            get
            {
                int result;
                result = Memory - comingFiles;
                return result;
            }
            set { comingFiles = value; }
            
        } //входящие параметры

        public int FreeMemoryInfo { get; set; }
        public string FullInfo { get; set; }

       

    }
}
